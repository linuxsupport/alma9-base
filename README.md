# AlmaLinux 9 Docker images

http://cern.ch/linux/almalinux/

## What is AlmaLinux 9 (ALMA9)?

Upstream AlmaLinux 9, with minimal patches to integrate into the CERN computing environment

### Image building

```
koji image-build alma9-docker-base `date "+%Y%m%d"` alma9-image-9x \
	http://linuxsoft.cern.ch/cern/alma/9/BaseOS/\$arch/os/ x86_64 aarch64 \
	--ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/alma9-base#master \
	--kickstart=alma9-base-docker.ks --ksversion=RHEL8 --distro=RHEL-9.0 --format=docker \
	--factory-parameter=dockerversion 1.10.1 \
	--factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
	--factory-parameter=docker_cmd '["/bin/bash"]' \
	--factory-parameter=generate_icicle False \
	--wait --scratch
```
